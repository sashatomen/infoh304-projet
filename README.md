# INFOH304 Projet

Groupe No XX
* NOM1 prénom1
* NOM2 prénom2
* NOM3 prénom3

## Instructions

1. Forkez ce projet en cliquant sur le bouton "Fork" en haut à droite
	* Pour "Visibility level", choisissez "Private"
	* Attention: cette opération ne doit être réalisée que par l'un des 3 membres du groupe
	* Instructions détaillées pour l'utilisation d'un fork: voir [le dépôt des TPs](https://gitlab.com/jeremieroland/infoh304)
1. Précisez votre numéro de groupe et vos noms en modifiant les données dans le README ci-dessus
1. Donnez accès en écriture à votre dépôt aux autres membres du groupe en les invitant en tant que "Maintainer" via le menu "Project Information -> Members"
1. Via le même menu, donnez accès en lecture à votre dépôt aux identifiants suivants en les nommant "Reporter" (attention, le rôle "Guest" ne donne pas accès aux fichiers sur le dépôt et n'est donc pas suffisant)
	* jeremieroland (Jérémie Roland, titulaire du cours)
	* Victorvicto (Victor Demuysere, assistant)
	* AFlachs (Alexandre Flachs, élève-assistant)
1. Communiquez l'adresse de votre dépôt via le [formulaire sur l'UV](https://uv.ulb.ac.be/mod/assign/view.php?id=882272)
1. Mettez à jour votre dépôt au fur et à mesure de votre travail sur le projet
1. Le dernier commit avant l'heure de l'échéance (intermédiaire ou finale) sera considéré comme la version soumise pour évaluation
